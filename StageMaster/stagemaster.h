#ifndef STAGEMASTER_H
#define STAGEMASTER_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>

namespace Ui {
class StageMaster;
}

class StageMaster : public QMainWindow
{
    Q_OBJECT

public:
    explicit StageMaster(QWidget *parent = nullptr);
    ~StageMaster();

private:
    Ui::StageMaster *ui;

    void setupServer();

    QTcpServer *_server;
    QList<QTcpSocket*> _hostList;

    struct Artist
    {
        QString name;
        double mic;
        double inst;
    };

    QMap<QString, Artist> _stageMap;

private slots:
    void recieveConnection();
    void readClientOut();
};

#endif // STAGEMASTER_H
