#include "stagemaster.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StageMaster w;
    w.show();

    return a.exec();
}
