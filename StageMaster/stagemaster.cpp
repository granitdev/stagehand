#include "stagemaster.h"
#include "ui_stagemaster.h"

#include <QDebug>

StageMaster::StageMaster(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StageMaster),
    _server(nullptr)
{
    ui->setupUi(this);
    setupServer();
}

StageMaster::~StageMaster()
{
    delete ui;
}

void StageMaster::setupServer()
{
    if(_server == nullptr) _server = new QTcpServer(this);
    connect(_server, &QTcpServer::newConnection, this, &StageMaster::recieveConnection);
    if(!_server->listen(QHostAddress::Any, 35795)) qDebug() << "server could not start";
    else qDebug() << "server waiting for connection...";
}

void StageMaster::recieveConnection()
{
    qDebug() << "recieved new connection";
    QTcpSocket *socket = _server->nextPendingConnection();
    _hostList.append(socket);
    connect(socket, &QTcpSocket::readyRead, this, &StageMaster::readClientOut);
}

void StageMaster::readClientOut()
{
    for(QTcpSocket *socket : _hostList)
    {




    }
}
