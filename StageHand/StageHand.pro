#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T18:37:22
#
#-------------------------------------------------

QT       += core gui network remoteobjects

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StageHand
TEMPLATE = app

INSTALLS        = target
target.files    = StageHand
target.path     = /home/pi


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        stagehand.cpp \
    artistwidget.cpp \
    addartistpopup.cpp \
    stageobject.cpp

HEADERS += \
        stagehand.h \
    artistwidget.h \
    addartistpopup.h \
    stageobject.h

FORMS += \
        stagehand.ui \
    artistwidget.ui \
    addartistpopup.ui \
    stageobject.ui

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
