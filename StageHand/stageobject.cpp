#include "stageobject.h"
#include "ui_stageobject.h"

StageObject::StageObject(const QString &name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StageObject)
{
    ui->setupUi(this);
    _name = name;
    ui->groupBox->setTitle(name);
}

StageObject::~StageObject()
{
    delete ui;
}

void StageObject::clearValue()
{
    ui->spinBox->clear();
}

void StageObject::on_upButton_clicked()
{
    ui->spinBox->stepUp();
    emit sigValueChanged(_name, ui->spinBox->value());
}

void StageObject::on_downButton_clicked()
{
    ui->spinBox->stepDown();
    emit sigValueChanged(_name, ui->spinBox->value());
}
