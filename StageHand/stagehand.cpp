#include "stagehand.h"
#include "ui_stagehand.h"

#include "artistwidget.h"
#include "addartistpopup.h"
#include <QtRemoteObjects>
#include <QDebug>

StageHand::StageHand(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StageHand),
    _socket(new QTcpSocket(this)),
    _timer(new QTimer(this))
{
    ui->setupUi(this);
//    QObject::connect(_timer, &QTimer::timeout, this, &StageHand::transmitMessage);

//    _timer->setInterval(3000);
    connectToMaster();
}

StageHand::~StageHand()
{
    delete ui;
}

void StageHand::on_addArtistButton_clicked()
{
    AddArtistPopup *aap = new AddArtistPopup(this);

    connect(aap, &AddArtistPopup::sigArtistConfig, this, &StageHand::addArtist);
    aap->show();
}

void StageHand::addArtist(const QVariantMap &vmap)
{
    ArtistWidget *aw = new ArtistWidget(this);
    connect(aw, &ArtistWidget::sigSendValues, this, &StageHand::transmitMessage);
    aw->setArtisValues(vmap);
    ui->mainHorzLayout->addWidget(aw);
}

void StageHand::connectToMaster()
{
    qDebug() << "attempting to connect to StageMaster";
    _socket->connectToHost("localhost", 35795);
//    _timer->start();
//    createMessage();
}

//void StageHand::createMessage()
//{
//
//    transmitMessage();
//}

void StageHand::transmitMessage(const QString message)
{
    qDebug() << "sending message";
    QByteArray ba = QString("test message").toLatin1();

}
