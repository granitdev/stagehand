#include "stagehand.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StageHand w;
    w.show();

    return a.exec();
}
