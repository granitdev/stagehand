#ifndef STAGEHAND_H
#define STAGEHAND_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTimer>

class ArtistWidget;

namespace Ui {
class StageHand;
}

class StageHand : public QMainWindow
{
    Q_OBJECT

struct Artist
{
    QString name;
    ArtistWidget *widget;
    double mic;
    double ins;
    double mon;
};

public:
    explicit StageHand(QWidget *parent = nullptr);
    ~StageHand();

private slots:
    void on_addArtistButton_clicked();
    void addArtist(const QVariantMap &vmap);
    void connectToMaster();
//    void createMessage();
    void transmitMessage(const QString message);

private:
    Ui::StageHand *ui;

    QTcpSocket *_socket;
    QTimer *_timer;
    QMap<QString, Artist> _artistMap;
};

#endif // STAGEHAND_H
