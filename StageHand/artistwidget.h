#ifndef ARTISTWIDGET_H
#define ARTISTWIDGET_H

#include <QWidget>
#include <QVariantMap>
#include <QLabel>

namespace Ui {
class ArtistWidget;
}

class ArtistWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ArtistWidget(QWidget *parent = nullptr);
    ~ArtistWidget();

signals:
    void sigSendValues(const QByteArray);
    void sigClearValues();

public slots:
    void setArtisValues(const QVariantMap &vmap);

private slots:
    void makeValueMap(const QString& name, const int & value);

    void on_sendButton_clicked();

private:
    Ui::ArtistWidget *ui;
    QLabel *_labelEditor;
    bool _editLock;

    void changeRequestValue(bool up);
    QVariantMap _valueMap;
    QString _name;
};

#endif // ARTISTWIDGET_H
