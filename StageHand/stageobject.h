#ifndef STAGEOBJECT_H
#define STAGEOBJECT_H

#include <QWidget>

namespace Ui {
class StageObject;
}

class StageObject : public QWidget
{
    Q_OBJECT

public:
    explicit StageObject(const QString &name, QWidget *parent = nullptr);
    ~StageObject();

public slots:
    void clearValue();

signals:
    void sigValueChanged(const QString&, const int&);

private slots:
    void on_upButton_clicked();
    void on_downButton_clicked();

private:
    Ui::StageObject *ui;

    QString _name;
};

#endif // STAGEOBJECT_H
