#ifndef ADDARTISTPOPUP_H
#define ADDARTISTPOPUP_H

#include <QWidget>
#include <QVariantMap>

namespace Ui {
class AddArtistPopup;
}

class AddArtistPopup : public QWidget
{
    Q_OBJECT

public:
    explicit AddArtistPopup(QWidget *parent = nullptr);
    ~AddArtistPopup();

signals:
    void sigArtistConfig(const QVariantMap);
    void sigCancelArtist();

private slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();
    void on_pushButton_clicked();

private:
    Ui::AddArtistPopup *ui;

    QVariantMap _vmap;
};

#endif // ADDARTISTPOPUP_H
