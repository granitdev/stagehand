#include "artistwidget.h"
#include "ui_artistwidget.h"
#include "stageobject.h"

#include <QVector>

ArtistWidget::ArtistWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ArtistWidget),
    _labelEditor(nullptr)
{
    ui->setupUi(this);

}

ArtistWidget::~ArtistWidget()
{
    delete ui;
}

void ArtistWidget::setArtisValues(const QVariantMap &vmap)
{
    QVector<QString> keys = vmap.keys().toVector();

    for(QString key : keys)
    {
        if(key == "name")
        {
            ui->groupBox->setTitle(vmap[key].toString());
        }
        else
        {
            if(vmap[key].toBool())
            {
                StageObject *so = new StageObject(key, this);
                ui->stageObjectsVertLayout->addWidget(so);
                connect(so, &StageObject::sigValueChanged, this, &ArtistWidget::makeValueMap);
                connect(this, &ArtistWidget::sigClearValues, so, &StageObject::clearValue);
            }
        }
    }
}

void ArtistWidget::makeValueMap(const QString &name, const int &value)
{
    _valueMap[name] = value;
}

void ArtistWidget::on_sendButton_clicked()
{
    emit sigSendValues(QByteArray());
    emit sigClearValues();
    _valueMap.clear();
}
