#include "addartistpopup.h"
#include "ui_addartistpopup.h"

#include <QDebug>
#include <QProcess>

AddArtistPopup::AddArtistPopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddArtistPopup)
{
    ui->setupUi(this);

}

AddArtistPopup::~AddArtistPopup()
{
    delete ui;
}

void AddArtistPopup::on_okButton_clicked()
{
    _vmap["name"] = ui->artistNameLineEdit->text();
    _vmap["vocals"] = ui->vocalsCheckbox->isChecked();
    _vmap["instrument"] = ui->instrumentCheckBox->isChecked();
    _vmap["monitor"] = ui->monitorCheckBox->isChecked();

    emit sigArtistConfig(_vmap);
    this->deleteLater();
}

void AddArtistPopup::on_cancelButton_clicked()
{
    emit sigCancelArtist();
    this->deleteLater();
}

void AddArtistPopup::on_pushButton_clicked()
{
    QProcess *p = new QProcess;
    p->start("matchbox-keyboard &");
}
